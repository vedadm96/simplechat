Simplechat project for web task

- authentication flow with keycloak (OAuth2)
- scope and role authority
- secured and unsecured endpoints for testing
- docker-compose file for setting up keycloak with a custom theme and realm import
- need to add users to test ( add member role to user to access member-service client )

TO-DO:
- database member and thread
- forum thread controllers
- refresh token
