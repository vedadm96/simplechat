package com.example.simplechat

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SimplechatApplication

fun main(args: Array<String>) {
	runApplication<SimplechatApplication>(*args)
}
