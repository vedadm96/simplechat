package com.example.simplechat.controller

import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class ApiController {

    @PreAuthorize("permitAll()")
    @GetMapping("/not-secured")
    fun getNonSecuredMessage() : String  {
        return "Server returned not secured message"
    }

    @GetMapping("/secured")
    fun getSecuredMessage() : String {
        return "Server returned secured message"
    }
}