package com.example.simplechat.config
import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.web.cors.CorsConfiguration

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
class SecurityConfig: WebSecurityConfigurerAdapter() {

    private val log = LoggerFactory.getLogger(this.javaClass)

    override fun configure(http: HttpSecurity) {
        log.info("Customizing security configuration (standard)")
        http.httpBasic()
                .disable()
                .csrf()
                .disable()
                .cors()
                .configurationSource{
                    val cors = CorsConfiguration()
                    cors.allowedOriginPatterns = listOf("http://localhost:4200")
                    cors.allowedMethods = listOf("GET","POST","PUT","DELETE")
                    cors.allowedHeaders = listOf("*")
                    cors
                }
                .and()
                .authorizeRequests()
                .antMatchers("/not-secured")
                .permitAll()
                .antMatchers("/secured")
                .hasAuthority("SCOPE_read")
                .anyRequest()
                .authenticated()
                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .oauth2ResourceServer()
                .jwt()
                .jwtAuthenticationConverter(KeycloakJwtAuthenticationConverter())

    }
}